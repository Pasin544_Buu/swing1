/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("My ActionListener: Action");
    }

}

/**
 *
 * @author Pla
 */
public class HelloMe implements ActionListener {

    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(500, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lbYourName = new JLabel("Your Name: ");
        lbYourName.setLocation(5, 5);
        lbYourName.setSize(80, 20);
        lbYourName.setBackground(Color.WHITE);
        lbYourName.setOpaque(true);

        JTextField txtYourName = new JTextField();
        txtYourName.setLocation(90, 5);
        txtYourName.setSize(200, 20);

        JButton btnHello = new JButton("Hello");
        btnHello.setLocation(90, 40);
        btnHello.setSize(80, 20);
        btnHello.setBackground(Color.RED);

        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);
        btnHello.addActionListener(new HelloMe());

        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class : Action");
            }
        };
        btnHello.addActionListener(actionListener);

        JLabel lbHello = new JLabel("Hello...", JLabel.CENTER);
        lbHello.setLocation(150, 100);
        lbHello.setSize(200, 20);
        lbHello.setBackground(Color.WHITE);
        lbHello.setOpaque(true);

        frmMain.setLayout(null);
        frmMain.add(lbYourName);
        frmMain.add(txtYourName);
        frmMain.add(btnHello);
        frmMain.add(lbHello);

        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtYourName.getText();
                lbHello.setText("Hello "+name);
            }

        });

        frmMain.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("HelloMe : Action");
    }
}
